function [result] = eqType(tspan,eul)
% EQTYPE function that determines whether the governing equation of a
% Euler angle's behavior is linear or sinusoidal and outputs the result. 
% Input: vector of Euler angles over time 
% Output: an array result of 0 or 1 indicating whether each Euler angle element is governed by a linear equation (0) or 
% or by a sinusoidal equation (1)

%% Pull angles from eul array
phi = eul(:,1);
theta = eul(:,2);
psi = eul(:,3);
t = tspan;

%% Find linear correlation coefficients of Euler Angles
clPhi = corrcoef(t(:,1),phi(:,1));
clTheta = corrcoef(t(:,1),theta(:,1));
clPsi = corrcoef(t(:,1),psi(:,1));

%% Find sinusoidal correlation coefficients of Euler Angles
csPhi = corrcoef(sin(t(:,1)),phi(:,1));
csTheta = corrcoef(sin(t(:,1)),theta(:,1));
csPsi = corrcoef(sin(t(:,1)),psi(:,1));

%% Initialize result
result = [0 0 0]';

%% Compare correlation coefficients

%To find whether the equation is linear or sinusoidal. Compares the 
%correlation between linear and sine. Cofficient with the
%largest value is designated as true
if abs(clPhi(2)) > abs(csPhi(2))
    result(1) = 0;
else 
    result(1) = 1;
end
if abs(clTheta(2)) > abs(csTheta(2))
    result(2) = 0;
else 
    result(2) = 1;
end
if abs(clPsi(2)) > abs(csPsi(2))
    result(3) = 0;
else 
    result(3) = 1;
end
end