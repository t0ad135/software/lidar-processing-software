function [eq1] = determineSine(tspan,y)
% DETERMINESINE takes inputs of data and time range, then uses a regression
% fit to determine the sinusoidal equation that describes the behavior of
% the euler angles as a function of time in the form y = A*sin(B*(x-h)) + k
% y = array of euler angle points
% t = array of timesteps
% eq1 = 
    N = length(tspan);
    fit = @(b,t)  b(1).*(sin(2*pi*t./b(2) + 2*pi/b(3))) + b(4); 
    fcn = @(b) sum((fit(b,tspan) - y).^2);  
    upper = max(y);
    lower = min(y);
    range = upper-lower;
    zeroing = y - upper + (range/2);
    %zero_crossing = t(zeroing.*circshift(zeroing,[0 1]) <= 0);
    %zero_crossing = fzero(zeroing,0);
    tol = 0.01;
    zerochecker = zeros(1,N);
    stopval = 0;
    for i = 1:N
        
       if (zeroing(i) >= (0-tol))&& (zeroing(i)<= (0+tol))&&(stopval == 0)
           zerochecker(i) = i;
           stopval = 1000;
       end
       if stopval ~= 0
            stopval = stopval - 1;
       end
    end
    zero_crossing = tspan(nonzeros(zerochecker));
    period = 2*mean(diff(zero_crossing));
    offset = mean(y);
    s = fminsearch(fcn, [range;  period;  -1;  offset]);
    trange = linspace(min(tspan),max(tspan),N);
    figure(1)
    hold on
    plot(trange,fit(s,trange),'r')
    eq1 = fit(s,trange);
    
end

