function [eulFuture] = propagateRotation(tspan,eul,dt)
%PROPAGATEROTATION takes in an array of euler angles and an array timespan,
%then outputs an array with predicted future euler angles over that
%timespan.
% eul = [[phi(1);...phi(end)],[theta(1);...theta(end)],[psi(1);...psi(end)]]
% tspan = [t1,t2]
% eulFuture = [[phi(1);...phi(end)],[theta(1);...theta(end)],[psi(1);...psi(end)]]
% t = [t1;...tend]

%% Create future t array
dtf = linspace(tspan(end)+dt(1),tspan(end)+dt(2),dt(2));

%% Determine function type
isSine = eqType(tspan,eul);

%% Allocate eulFuture
eulFuture = zeros(length(dtf),4);
eulFuture(:,4) = dtf;
%% Fit data to equation then propagate
% Check whether sine or linear function
for i = 1:3
if isSine
    eq = zeros(4,3);
        eq(:,i) = determineSine(eul(:,i),tspan');
        for j = 1:length(dtf)
            eulFuture(j,i) = eq(1,i)*sin(eq(2,i)*(dtf(j)-eq(3,i)))+eq(4,i);
        end
else 
    eq = zeros(2,3);
        eq(:,i) = determineLinear(eul(:,i),tspan');
        for j = 1:length(dtf)
            eulFuture(j,i) = eq(1,i)*dtf(j) + eq(2,i);
        end
end
end
