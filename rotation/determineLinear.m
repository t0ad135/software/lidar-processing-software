function [eq1] = determineLinear(tspan,y)
%Input:Data and Load
%load = x (must be a column)
%data = y (must be a column)
%x_data(1) = m term
%x_data(2) = b term
%Output:Best Fit Variables
%Purpose: Find Best fit and how accurate it is to the data
%Method: Use Least Squares and linear algebra analysis
y = y';
n = ones(numel(tspan),1); 
    leastsquaresload = [tspan,n];
    eq1 = (leastsquaresload'*leastsquaresload)\(leastsquaresload'*y);
end