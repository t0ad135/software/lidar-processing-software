# LiDAR Processing Software
Repository for LiDAR processing code

# Authors
Nicholas Herrington
Brandon DiLorenzo
Jason Le
Shawn Stone
Jianai Zhao
Tyler Gaston
Zackary Hubbard
Jash Bhalavat
Jake Pirnack
Walter Sabin
Max Morgan

# About
The Project CROACS servicer software is contained within this repository. The purpose of this program is to measure and analyze the orientation of a client object that is tumbling about multiple axes. It uses LiDAR to create point clouds of the client object, which are matched to a template of possible orientations. The orientations of the point cloud data are determined and used to predict the future motion of the tumble. Figures and visualizations are output.

# Dependencies
The CROACS software uses the aditof sdk to obtain images from the LiDAR sensor. The aditof sdk git page can be viewed here:
https://github.com/analogdevicesinc/aditof_sdk

MATLAB and Python are required to run the program modules.

# Usage
The CROACS application requires little user interaction. To run and analyze a tumble scenario, first run the bash script '/lidar-processing-software/shellScripts/launchSensor.sh'. This will prompt the user with necessary framerate and analysis duration parameters. Input parameters. Once the parameters are entered, the rest of the CROACS module will continue to run without user input. The user will be presented with figures, an output file of client state vector predictions, and a 3D visualization of the client's tumble. All resulting files will be saved to the "active" directory as well as the "archive" directory.

# File Structure
## active
This directory contains files generated and used by the most recent run of the program. The contents of this folder are automatically deleted preceding each run of the program. 

## Rotation
The rotation folder contains the tumble prediction and visualization modules of the program.

## Processing
The processing folder contains the import, denoising, template matching, and angle characterization modules of the program.

## shellScripts
The shellScripts folder contains bash scripts which serve to connect the different modules of the CROACS software.

## archive
The archive folder is where point cloud data imported from the LiDAR sensor is stored. Subfolders are automatically named based on the date and time at which their contained point clouds and timestamps were created. This directory also contains a folder with simulated point cloud data.

## ShowPointCloud
The showPointCloud folder contains a python module which imports point cloud data from the LiDAR sensor.

## Calibration
The Calibration folder is extraneous to the main run of the program. It contains a calibration program which determines the geometrical lens properties of the attached LiDAR sensor if they are desired.

# Process
This section explains the methodology of our processing software by walking through a single run of the application.

## launchSensor.sh
When run, this shell script activates the Anaconda python environment necessary to run the program. It then calls the second module of the program, showPointCloud.py.

## showPointCloud.py
This module creates the necessary directories for point cloud storage, then accepts user inputs for framerate and test duration. It then enters a loop for the duration specified and captures point clouds from the sensor at the specified framerate. When the capture process has finished, it calls the next module launchProcessing.sh.

## launchProcessing.sh
This shell script simply calls the next module, croacsMain.m

## croacsMain.m
This matlab script is the main body of the processing module. It first takes in point clouds from the "active" directory and performs noise reduction by adding a bounding box around the client, outside of which any points are ignored. It also uses a noise filtering function. Once this is completed the centroid of the point cloud is calculated, and the points are then translated to be centered at the origin. The next step is that the program creates a template database of the client at a range of angles for matching reference. Once this is completed, the active point cloud is compared to the template database and matched to the attitude it most resembles. Relative velocity and rotational velocity are then differentiated from the point cloud position data. This data is then written to a file. The propagateRotation function is then called, which returns the predicted future attitude of the client. The future attitudes are then written to the file eulFuture.csv. The next module rotation.py is then called.

### propagateRotation.m
This function takes in the euler angles determined from template matching as well as a propagation time given by the user. It finds a best-fit line for the attitudes as a function of time, then calls the eqType function to see whether the tumble most resembles a linear or sinusoidal function. It does this for each subsequent axis individually. Once the function type is determined, it then submits the attitudes to either the determineSine or determineLinear function, which propagates the angles forward based on the determined equation coefficients from the attitude best-fit line.

## rotation.py
This module constructs a model of the client using vpython libraries. It then reads in predicted attitudes from eulFuture.csv. These attitudes are then represented in sequence in a figure window.  
