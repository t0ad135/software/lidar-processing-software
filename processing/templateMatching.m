function correlation = templateMatching(sensorPointCloud, database_templates)
% This function will execute the template matching algorithm for a given
% point cloud scanned by the LiDAR sensor. The sensed point cloud will go
% through each template in the database to determine its correlation

% Inputs:
% sensorPointCloud - acquired point cloud of client body
% PtCloudDatabase - point cloud database containing client at different
% orientations
%
% Outputs:
% correlation - array containing the correlation of the sensed point cloud
% to each template in the database. A lower correlation value corresponds
% to a higher degree of matching.

% template = pcread("ClientBody_v7.ply");
% delta = 10;
% roll = -180:delta:180;
% pitch = -90:delta:90;
% yaw = -180:delta:180;
% roll = roll(1:end-1);
% pitch = pitch(1:end-1);
% yaw = yaw(1:end-1);
% 
% [database_angles, database_templates] = templateDatabase(template, roll, pitch, yaw);
% 
% q = 0;
% str1 = '/home/croacs/Desktop/lidar-processing-software/archive/Simulated Data/run1/pc';
% str2 = num2str(q);
% str3 = '.ply';
% sensorPointCloud = pcread(append(str1,str2,str3));
% 
% pcshow(sensorPointCloud)
%% Function starts here
% Grab point cloud coordinates and number of points
sensorPointCloud2 = cloudSorting(sensorPointCloud);
sensor_coords = double(sensorPointCloud2.Location);
sensor_Np = double(sensorPointCloud2.Count);

%sensor_coords(:,2) = 0;

%Calculate the number of templates/orientations
[m,n,p] = size(database_templates);
N = m*n*p;

%For each template in the database, run through the template matching
%algorithm and save a correlation value to an array
correlation = zeros([1,N]);
parfor i = 1:N
%parfor i = 1:N
    ptCloud = database_templates{i};
    ptCloud2 = cloudSorting(ptCloud);
    template_coords = double(ptCloud2.Location);
    template_Np = double(ptCloud2.Count);
    difference = 0;
    
    if template_Np >= sensor_Np
        Np = sensor_Np;
    else
        Np = template_Np;
    end

    % For each point in the sensed point cloud, calculate the difference
    % between the sensed point and the template point
    for j = 1:Np
        difference = difference + (norm(sensor_coords(j,:) - template_coords(j,:)))^2;
    end
    correlation(i) = difference/Np;
end


end