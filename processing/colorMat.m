function [color_matrix] = colorMat(xyzpoints, RGBbase_color)
%This function takes in an Nx3 matrix of xyz coordinates and a 1x3 RGB base color (0-255), and outputs a
%color matrix of size Nx3x3, witch maps each point to RGB values. By default, a
%gradient is applied to the color in the y direction (depth).

%INPUTS:
%   xyzpoints = Nx3 matrix
%   RGBbase_color = 1x3 vector with values between 0-255

%OUTPUTS:
%   color_matrix = Nx3x3 matrix with gradient in y direction



% base_color = RGBbase_color/255; %normalizing to range of [0 1]
range = [0 1]; %range for normalization of the xyzpoints matrix
dir = 2; %x=1, y=2, z=3

color_matrix = uint8(zeros(size(xyzpoints)));
for i=1:3
    %adds 3 values for each point with a nominal value set to the base color
    color_matrix(:,:,i) = RGBbase_color(i);
end

%vector math for normalization (to get xyzpoints scaled to the desired range)
scale = (max(range) - min(range)) ./ (max(xyzpoints(:,dir)) - min(xyzpoints(:,dir)));
gradient_vec = xyzpoints(:,dir).*scale - min(xyzpoints(:,dir)).*scale + min(range);

color_matrix(:,dir,:) = uint8(gradient_vec*RGBbase_color);
end