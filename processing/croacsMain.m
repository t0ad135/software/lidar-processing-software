% Brandon diLorenzo
% Nicholas Herrington
% Shawn Stone
% Last Modified: 4/3/22

%% Housekeeping
clear
close all
clc;

%% Add folders to PATH
strPath = '/home/croacs/Desktop/lidar-processing-software';
addpath(genpath(strPath));
rehash

%% Get Point Cloud for Template Database
clientBodyRef = pcread('ClientBody_v7.ply');
refCoords = double(clientBodyRef.Location);
% Scale to desired dimensions
%refCoords = refCoords / 1.4e3;
centroidTemp = centroidComputation(refCoords);
coordsTemp = refCoords - centroidTemp;
cbr = pointCloud(coordsTemp);

%% Get Point Cloud Data and Process

% How many clouds?
%files = dir('/home/croacs/Desktop/lidar-processing-software/active/*.ply');
%files = dir('/home/croacs/Desktop/lidar-processing-software/archive/041220221924/clouds/*.ply');
files = dir('/home/croacs/Desktop/lidar-processing-software/archive/Simulated Data/run21/*.ply');
f = numel(files);


%str1 = '/home/croacs/Desktop/lidar-processing-software/active/pc';
%str1 = '/home/croacs/Desktop/lidar-processing-software/archive/041220221924/clouds/pc';
str1 = '/home/croacs/Desktop/lidar-processing-software/archive/Simulated Data/run21/pc';
str3 = '.ply';

% Set iterator
q = 0;

position_array = [];
attitude_array = [];
adjusted_attitude = [];
while q < f
    
    %Make sure that all the data has been collected
%     if q == 10
%         files = dir('/home/croacs/Desktop/lidar-processing-software/active/*.ply');
%         f = numel(files);
%     end

    % Read in data
    str2 = num2str(q);
    filePath = append(str1,str2,str3);
    ptCloud = pcread(filePath);

    % Plots point cloud
%     figure(1)
%     pcshow(ptCloud, 'BackgroundColor','w');
%     title('Scanned Point Cloud')
%     xlabel('X [m]')
%     ylabel('Y [m]')
%     zlabel('Z [m]')

    % Extract any desired data from point cloud
    original_coords = double(ptCloud.Location);
    Np = double(ptCloud.Count);
    
    %% Geometrical Noise Reduction
    if ~exist('/home/croacs/Desktop/lidar-processing-software/active/posServicer.csv') 
        trans_enc = [0,0,0];
    else
        trans_enc = readmatrix('/home/croacs/Desktop/lidar-processing-software/active/trans_enc.csv');
    end

    xBounds = [-0.3,0.3]; % bounding box x axis
    xBounds = xBounds + trans_enc(1);
    yBounds = [-0.5,0.5]; % bounding box y axis
    yBounds = yBounds + trans_enc(2);
    zBounds = [-0.15,0.3]; % bounding box z axis
    zBounds = zBounds + trans_enc(3);
    

    % Apply sensor movement using dx = x + x_enc
    translated_coordinates = original_coords + trans_enc;

    if isempty(translated_coordinates)
        !/home/croacs/Desktop/lidar-processing-software/shellScripts/launchSensor.sh
    end

    % Iterate through and apply these bounds to eliminate points that are
    % outside the bounding box
    i = 1;
    while i <= length(translated_coordinates(:,1))
        % xBounds
        if translated_coordinates(i,1) < xBounds(1)
            translated_coordinates(i,:) = [];
            i = i-1;
        elseif translated_coordinates(i,1) > xBounds(2)
            translated_coordinates(i,:) = [];
            i = i-1;
        end
        i = i+1;
    end

    j = 1;
    while j <= length(translated_coordinates(:,1))
        % yBounds
        if translated_coordinates(j,2) < yBounds(1)
            translated_coordinates(j,:) = [];
            j = j-1;
        elseif translated_coordinates(j,2) > yBounds(2)
            translated_coordinates(j,:) = [];
            j = j-1;
        end
        j = j+1;
    end

    k = 1;
    while k <= length(translated_coordinates(:,1))
    % zBounds
        if translated_coordinates(k,3) < zBounds(1)
            translated_coordinates(k,:) = [];
            k = k-1;
        elseif translated_coordinates(k,3) > zBounds(2)
            translated_coordinates(k,:) = [];
            k = k-1;
        end

        k = k + 1;
    end

    ptCloudTranslated = pointCloud(translated_coordinates);
    
    %pcshow(ptCloudTranslated,'BackgroundColor','w')

    %% Use noise filtering functions

    %ptCloud1 = pcdenoise(ptCloudTranslated,'NumNeighbors',4,'Threshold',0.2);
    ptCloud1 = ptCloudTranslated;
    

    %% Centroid Determination and Translation to Origin

%     coordinates = double(ptCloudCentroid.Location);
    coordinates = double(ptCloud1.Location);

    % Find the centroid coordinates by taking the average of all points
    centroid = centroidComputation(coordinates);

    % Move the point cloud to be centered at the origin
    coordinates = coordinates - centroid;
    ptCloudCentroid = pointCloud(coordinates);

    % Plots point cloud
%     figure(2)
%     pcshow(ptCloudCentroid,"BackgroundColor",'w','MarkerSize',100);
%     title('Processed Point Cloud')
%     xlabel('X [m]')
%     ylabel('Y [m]')
%     zlabel('Z [m]')

    %% Euler Angle Ranges and Step Size
            
    if q == 0
         % Angular Step Size for Templates
        delta = 10;                                                                                                                                                                  
    
        % Euler Angles
        roll = -180:delta:180;
        pitch = -90:delta:90;
        yaw = -180:delta:180;
        %roll = 0;
        %pitch = 0;
        %yaw = 0;
    
        roll = roll(1:end-1);
        pitch = pitch(1:end-1);
        yaw = yaw(1:end-1);
    else
        delta = 1;
        base_angles = attitude_array(q,:);

        roll = base_angles(1)-10:delta:base_angles(1)+10;
        pitch = (base_angles(2)-10):delta:(base_angles(2)+10);
        yaw = base_angles(3)-10:delta:base_angles(3)+10;
        %roll = 0;
        %pitch = 0;
        %yaw = 0;

        roll = roll(1:end-1);
        pitch = pitch(1:end-1);
        yaw = yaw(1:end-1);
    end

    %% Create Big Template Database

    [database_angles, database_templates] = templateDatabase(cbr, roll, pitch, yaw);


    %% Initial Template Matching

    correlation = templateMatching(ptCloudCentroid, database_templates);


    %% Getting Attitude from Matched Template
    min_correlation = min(correlation);
    matched_idx = find(correlation == min_correlation);

    matched_angles = zeros([length(matched_idx),3]);
    for i = 1:length(matched_idx)
        matched_angles(i,:) = database_angles{matched_idx(i)};
    end
    
%     if isempty(matched_angles)
%         clear
%         close all
%         clc;
%         run('/home/croacs/Desktop/lidar-processing-software/processing/croacsMain.m')
%     end

    %% Adjust angle outputs to bounds

    xroll = matched_angles(1);
    ypitch = matched_angles(2);
    zyaw = matched_angles(3);

    nX = fix(xroll/360);
    nY = fix(ypitch/180);
    nZ = fix(zyaw/360);

    xroll = xroll - nX*360;
    ypitch = ypitch - nY*180;
    zyaw = zyaw - nZ*360;

    new_matched_angles = [xroll, ypitch, zyaw];

    %% Save outputs and clear workspace for next frame

    % Increase iterator
    q = q + 1;

    % Save desired outputs
    position_array(q,:) = centroid; %recorded in meters
    attitude_array(q,:) = matched_angles(1,:); %recorded in degrees
    adjusted_attitude(q,:) = new_matched_angles;

    
    % Clear variables
    clearvars -except cbr f q str1 str3 position_array attitude_array adjusted_attitude
    
end

%% Differentiate velocites from position and attitude
fid = fopen('/home/croacs/Desktop/lidar-processing-software/active/time.txt');
cac = textscan( fid, '%s%f%f%f%f', 'CollectOutput', true );
sts = fclose( fid );
time_matrix = datevec( char(cac{1}), 'HH:MM:SS.FFF' );
time_array = time_matrix(:,6) + time_matrix(:,5)*60;

[prows,pcols] = size(position_array);
[arows,acols] = size(attitude_array);

time_diff = zeros(60-1,1);
vel_trans = zeros(prows-1,pcols);
vel_rot = zeros(arows-1,acols);
for i = 1:length(time_diff)
    time_diff(i) = time_array(i+1) - time_array(i);
    vel_trans(i,:) = (position_array(i+1,:) - position_array(i,:)) ./ time_diff(i);
    vel_rot(i,:) = (attitude_array(i+1,:) - attitude_array(i,:)) ./ time_diff(i);
end

%tdiff = time_diff(length(time_diff) -  time_diff(1));


%% Write data to file

data = zeros([prows,2*pcols+acols]);
data(:,1:pcols) = position_array;
data(:,pcols+1:pcols+acols) = adjusted_attitude;

fprintf("Point Clouds Analyzed - On to Post-Processing \n")

%writematrix(data, '/home/croacs/Desktop/lidar-processing-software/active/data.csv')

%% Plots for presenting

files = 1:59;


true_pos = 0;
mean_pos = mean(vel_trans(:,2));
std_pos = std(vel_trans(:,2));

% figure(1)
% scatter(files, vel_trans(:,2))
% xlabel('Point Cloud Frames');ylabel('Velocity (m/s)'); ylim([-1e-1,1e-1]);
% hold on
% yline(mean_pos,'Color','b')
% yline(true_pos,'Color','g')
% yline(mean_pos + std_pos,'Color','r')
% yline(mean_pos - std_pos,'Color','r')
% title('Translation Velocity of Client')
% legend('Estimates','Mean Estimate',"Truth",'\pm 1 Std Dev','Location','best')

true_att = [0,7,-45];
mean_att = mean(vel_rot,1);
std_att = std(vel_rot);

figure(2)
scatter(files,vel_rot(:,3),'filled','SizeData',15,'CData',[0 0 0])
xlabel('Point Cloud Frames');ylabel('Velocity (deg/s)'); 
ylim([true_att(3)-35,true_att(3)+30])
hold on
yline(mean_att(3),'Color','b','LineWidth',2)
yline(true_att(3),'--','Color','g','LineWidth',2)
yline(mean_att(3) + 1*std_att(3),'Color','r','LineStyle','--')
yline(mean_att(3) + 2*std_att(3),'Color','r')
yline(mean_att(3) - 1*std_att(3),'Color','r','LineStyle','--')
yline(mean_att(3) - 2*std_att(3),'Color','r')
title('Z-Axis Rotational Velocity of Client')
legend('Estimates from Simulated Data','Mean','Truth','\pm 1 Std Dev','\pm 2 Std Dev','Location','best')


files = 1:60;
figure(3)
scatter(files,adjusted_attitude(:,3))

%% Propagate
attitude_array = table2array(Run21SimAttitude);
eul = attitude_array;
%eul = adjusted_attitude;
time = time_array(1:60);

dtr = [1 5000]; % get this time range from showPointCloud user input

eulFuture = propagateRotation(time,eul,dtr);
writematrix(eulFuture,'/home/croacs/Desktop/lidar-processing-software/active/eulFuture.csv');

%%
% call the next module
!python /home/croacs/Desktop/lidar-processing-software/rotation/rotation.py