function ptCloudOut = cloudSorting(ptCloudIn)
% This function will organize the points contained within a point cloud.
% The points will be sorted in ascending order of the normal of the XYZ
% coordinates. This function hopes to help organize for the template
% matching algorithm 

% Inputs:
% ptCloudIn - Point cloud in need of sorting
%
% Outputs:
% ptCloudOut - Sorted point cloud; points in ascending order by magnitude

%ptCloudIn = pcread("ClientBody_v7.ply");

%Extract origional coordinates from the point cloud
og_coordinates = double(ptCloudIn.Location);

%Determine the magnitude of each point
% magnitudes = vecnorm(og_coordinates,2,2);

%Sort the magnitudes in ascending order; grab the sorted indices
[x, indicesX] = sort(og_coordinates(:,1));
[y, indicesY] = sort(og_coordinates(:,2));
[z, indicesZ] = sort(og_coordinates(:,3));

%Sort the coordinates based on the sorted magnitudes
sorted_coordinates = og_coordinates(indicesX,:);
[rows,col] = size(sorted_coordinates);

final_coordinates = zeros(length(x),3);
count = 1;
i = 1;

while i <= rows
    [rows,col] = size(sorted_coordinates);
    if (rows~=0)
        yFind = find(sorted_coordinates(:,2) <= sorted_coordinates(i,2));
        zFind = find(sorted_coordinates(:,3) <= sorted_coordinates(i,3));
        if ~isempty(yFind)
            if ~isempty(zFind)
                final_coordinates(count,:) = sorted_coordinates(i,:);
                sorted_coordinates(i,:) = [];
                i = 0;
                count = count+1;
            end
        end
    else
        break
    end
    i = i+1;
end

first = final_coordinates(1,:);
last = final_coordinates(end,:);
%Write the sorted coordinates to a point cloud
ptCloudOut = pointCloud(final_coordinates);

% pcshow(ptCloudOut)
% xlabel('x');ylabel('y');zlabel('z')
% hold on
% scatter3(first(1),first(2),first(3),'red','filled')
% scatter3(last(1),last(2),last(3),'white','filled')


end

