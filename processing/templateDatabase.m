function [database_angles, database_templates] = templateDatabase(ptCloud, roll, pitch, yaw)
% This function serves as the Database Creator for the updated Template
% Matching algorithm. 

% Inputs: 
% ptCloud - Point Cloud of desired client (point cloud format)
% roll, pitch, yaw - range of angles for client orientation
%
% Outputs:
% database_angles - struct of angles used
% database_templates - struct of point cloud at different orientations

% Grab point cloud coordinates and number of points
coordinates = double(ptCloud.Location);
Np = double(ptCloud.Count);

% Allocate cell arrays for the angles and point cloud coordinates at
% different orientations
database_angles = cell(length(roll),length(pitch),length(yaw));
database_templates = cell(length(roll),length(pitch),length(yaw));

%For each unique angle of roll, pitch, and yaw, create a database of each
%orientation in the given angle ranges
for ii = 1:length(roll)
    for jj = 1:length(pitch)
        for kk = 1:length(yaw)
            database_angles{ii,jj,kk} = [roll(ii),pitch(jj),yaw(kk)];
        end
    end
end

%Calculate the number of templates/orientations
[m,n,p] = size(database_angles);
N = m*n*p;

%For each roll-pitch-yaw combination, transform the point cloud to the
%desired orientation and save to the database
parfor i = 1:N
    quat = quaternion(flip(database_angles{i}),'eulerd','ZYX','point');
    
    matrix_q = quat2rotm(quat);
    
    new_coords = zeros(size(coordinates));
    for mm = 1:Np
        new_coords(mm,:) = matrix_q * coordinates(mm,:)';
    end

    indices = find(new_coords(:,2) > 0);
    new_coords(indices,:) = [];

    centroid = centroidComputation(new_coords);
    coords = new_coords - centroid;
    %coords = new_coords;
    
    ptCloudNew = pointCloud(coords);
    database_templates{i} = ptCloudNew;
end


end