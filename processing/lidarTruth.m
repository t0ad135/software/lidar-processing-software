% LiDar DATA processign ticket #22 -Jianai Zhao
clear all; close all; clc;

%% Actual data from encoder 
 actualdata  = readtable('/home/croacs/Desktop/lidar-processing-software/active/truth/truthData'); %- txt file from Jash, only corrected row are kept 
 actualdata = table2cell(actualdata);
 actualdata = actualdata(2:end,:);
 actualdata= actualdata(2:2:end,:);
 actualdata = actualdata(:,3);
 actualdata = cell2mat(actualdata);

 %% Predicted data from point cloud 
 prediciteddata = load('eul.csv');
 

%% PLOT 
figure 
plot([0:785:8.68],prediciteddata(:,1))
hold on 
plot([0:785:8.68],actualdata(1:785,1))

