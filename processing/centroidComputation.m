function centroid = centroidComputation(coordinates)

mu_x = mean(coordinates(:,1));
mu_y = mean(coordinates(:,2));
mu_z = mean(coordinates(:,3));
centroid = [mu_x, mu_y, mu_z];