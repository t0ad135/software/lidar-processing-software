% Simulated Data Testing

clear all
close all
clc

%%

ptCloud = pcread('ClientBody_v7.ply');
%ptCloud = pcread('RocketTest.ply');

% Plots point cloud
figure
pcshow(ptCloud,"BackgroundColor",'w');
title('Original Point Cloud')

% Extract any desired data from point cloud
original_coordinates = double(ptCloud.Location);
Np = double(ptCloud.Count);


centroid = centroidComputation(original_coordinates);
% Move the point cloud to be centered at the origin
translated_coordinates = original_coordinates - centroid;

ptCloudCentroid = pointCloud(translated_coordinates);


%% Randomly Downsample

% ptCloudRandom = pcdownsample(ptCloudCentroid,"random",0.2);
% total_coordinates = double(ptCloudRandom.Location);
% 
% figure
% pcshow(ptCloudRandom)

%pcwrite(ptCloudRandom,'SimulatedData_v1.ply')


%% Noise

% for i = 1:(Np/3)
% 
%     coordinates = translated_coordinates(3*i,:);
% 
%     coordinates = coordinates + 20*rand([1,3]) - 20*rand([1,3]);
% 
%     noisy_coords(i,:) = coordinates; 
% end
% 
% total_coordinates = [translated_coordinates;noisy_coords];
% 
% ptCloudNoise = pointCloud(total_coordinates);
% figure
% pcshow(ptCloudNoise)


%% Rotation

runtime = 15; %seconds

initial = [0,0,0];
w = [0,0,2];
fps = 4;
str1 = '/home/croacs/Desktop/lidar-processing-software/archive/Simulated Data/run21/pc';

database_templates = {};
database_angles = {};
parfor i = 1:runtime*fps

    database_angles{i} = initial + w*(i-1)/fps

    quat = quaternion(flip(database_angles{i}),'eulerd','ZYX','point');
    
    matrix_q = quat2rotm(quat);
    
    new_coords = zeros(size(translated_coordinates));
    for mm = 1:length(translated_coordinates)
        new_coords(mm,:) = matrix_q * translated_coordinates(mm,:)';
    end

    indices = find(new_coords(:,2) > 0);
    new_coords(indices,:) = [];

    ptCloudNew = pointCloud(new_coords);
    database_templates{i} = ptCloudNew;
end



%str1 = '/home/croacs/Desktop/lidar-processing-software/archive/Simulated Data/pc';
str3 = '.ply';

for i = 1:length(database_templates)
    str2 = num2str(i-1);
    filePath = append(str1,str2,str3);

    pcwrite(database_templates{i},filePath)
end


%%

images = length(database_templates);
fig = figure;
im = {};
for idx = 1:images
    if idx == 1
        plot = pcshow(database_templates{idx},'BackgroundColor','w');
        xlabel('X'); ylabel('Y'); zlabel('Z')
        %set(gca, 'XLim', [-400,400],'YLim', [-400,0],'ZLim', [-400,400])
        %axes = [plot.XLim,plot.YLim,plot.ZLim];
        %set(gca, 'XLim', [axes(1),axes(2)],'YLim', [axes(3),axes(4)],'ZLim', [axes(5),axes(6)])
    else
        pcshow(database_templates{idx},'BackgroundColor','w');
        xlabel('X'); ylabel('Y'); zlabel('Z')
        %set(gca, 'XLim', [-400,400],'YLim', [-400,0],'ZLim', [-400,400])
    end
    drawnow
    frame = getframe(fig);
    im{idx} = frame2im(frame);
end
close;
filename = 'Client.gif'; % Specify the output file name
for idx = 1:images
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0.2);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.2);
    end
end


















