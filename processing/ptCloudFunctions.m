% This script contains useful point cloud functions that we are not
% currently using in the main script. Keep this as a reference document


%% Downsampling

% Random
ptCloudRandom = pcdownsample(ptCloudCentroid,'random',0.5);
figure
pcshow(ptCloudRandom,"BackgroundColor",'w','MarkerSize',100);
title('Downsampled Point Cloud - Random')

% Notes: This method is the most efficient and easiest to control, but
% there is no knowledge available to how it gets rid of the points. If we
% could find this knowledge, I would be very comfortable using this method.
% Another note, if this were to be selected, how would it impact any
% uniques features on the object? Something to think about for future


% Grid Average
gridStep = 0.0025;

ptCloudGrid = pcdownsample(ptCloudCentroid,'gridAverage',gridStep);
figure
pcshow(ptCloudGrid,"BackgroundColor",'w','MarkerSize',100);
title('Downsampled Point Cloud - Grid Average')

% Notes: gridStep acts as the resolution of the point cloud. The higher the
% grid step, the less points in the point cloud. I like knowing what filter
% this method uses, however, I don't know how we would decide what
% resolution to use for each LiDAR data set. We could have a hard-coded
% value for each point cloud, or have a flexible range based on parameters
% that we decide


% Nonuniform Grid Sample
maxNumPoints = 10;

ptCloudNonuniform = pcdownsample(ptCloudCentroid,'nonuniformGridSample',maxNumPoints);
figure
pcshow(ptCloudNonuniform,"BackgroundColor",'w','MarkerSize',100);
title('Downsampled Point Cloud - Nonuniform Grid Sample')

% Notes: maxNumPoints acts as the resolution of this method. The higher the
% number of points, the less points in the point cloud. I don't like this
% method much at all. The most minimum maxNumPoints is 6, which doesn't
% produce many points in the first place. The only positive is knowing what
% type of filter it uses

%% Denoise

% Denoise the point cloud using available neighborhood values within a
% specified radius
pointCloud2 = pcmedian(ptCloudCentroid,'Dimensions',3,'Radius',0.05);
pointCloud2 = pcmedian(ptCloudCentroid);

figure
pcshow(pointCloud2,"BackgroundColor",'w','MarkerSize',100);
title('Point Cloud - Median')

pointCloud = pointCloud2;




