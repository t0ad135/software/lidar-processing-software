% Script for viewing captured point clouds without ruinning the entire main
% script

files = dir('/home/croacs/Desktop/lidar-processing-software/active/*.ply');
%files = dir('/home/croacs/Desktop/lidar-processing-software/archive/040320221103/clouds/*.ply');
f = numel(files);
%030320220927

%str1 = '/home/croacs/Desktop/lidar-processing-software/active/pc';
str1 = '/home/croacs/Desktop/lidar-processing-software/archive/041220221924/clouds/pc';
str3 = '.ply';

% Set iterator
q = 0;

for i = 1:1
    % Read in data
    str2 = num2str(q);
    filePath = append(str1,str2,str3);
    ptCloud = pcread(filePath);
    coords = ptCloud.Location;
    % ptCloud = pcread("ClientBody_v6.ply");
    
    figure(1)
    pcshow(ptCloud);
    title('Original Point Cloud')
    xlabel('x')
    ylabel('y')
    zlabel('z')

    q = q+1;
end

% ptCloudDown = pcdownsample(ptCloud,"gridAverage",0.0075)
% figure
% pcshow(ptCloudDown)
% pcwrite(ptCloudDown,'ClientBody_v7.ply')
